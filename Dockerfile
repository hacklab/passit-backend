FROM python:3.6-alpine
ENV PYTHONUNBUFFERED 1
ENV PORT 8080

RUN mkdir /code
WORKDIR /code

RUN apk add --update --no-cache \
  libsodium-dev \
  postgresql-dev \
  libffi-dev \
  gcc \
  musl-dev \
  linux-headers

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8080

COPY . /code/

CMD ["./bin/start.sh"]
